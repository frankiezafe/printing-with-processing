# README #

### Utilities to print with processing ###

* Set of processing.org sketches to send texts or images to a printer.

* Created and managed by [frankie zafe](http://www.frankiezafe.org)
* [Openprocessing](http://www.openprocessing.org/user/5359)
