// il faut d'abord lister les imprimantes disponibles, ensuite sélectionner la bonne et lancer l'impression

void setup() {
  // 1. > voir le nom des imprimantes disponibles
  listPrinters();
  // dans la console, le nom des imprimantes s'imprime
  // 2. récupérer l'imprimante par son nom, chez moi, c'est 'Hewlett-Packard-HP-Color-LaserJet-CP1515n'
  PrintService imprimante = getPrinterByName("Hewlett-Packard-HP-Color-LaserJet-CP1515n");
  // on teste si ça a fonctionné
  if ( imprimante != null ) {
    // 3. on lance l'impression en spécifiant quelle imprimante utiliser
    printText( imprimante, "Mon texte à imprimer" );
    // tu peux lancer autant d'impression que tu veux sur 'imprimante'
    // tu peux aussi créer une variable 'PrintService imprimante2' qui a un autre nom.
    // normalment...
  } else {
    println("Impossible de connecter cette imprimante!");
  }
}
