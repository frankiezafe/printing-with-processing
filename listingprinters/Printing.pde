// based on: http://www.java2s.com/Tutorial/Java/0261__2D-Graphics/javaxprintAPIandallowsyoutolistavailableprintersqueryanamedprinterprinttextandimagefilestoaprinterandprinttopostscriptfiles.htm
import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.JTextPane;

public void listPrinters() {
  PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
  attributes.add( MediaSizeName.ISO_A4 );
  // Find all services that can support the specified attributes
  PrintService[] services = PrintServiceLookup.lookupPrintServices(null, attributes);
  // Loop through available services
  for (int i = 0; i < services.length; i++) {
    System.out.println(services[i].getName());
  }
}

public PrintService getPrinterByName( String name ) {
  PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
  attributes.add( MediaSizeName.ISO_A4 );
  PrintService[] services = PrintServiceLookup.lookupPrintServices(null, attributes);
  for (int i = 0; i < services.length; i++) {
    if ( services[i].getName().equals( name ) ) {
      return services[i];
    }
  }
  return null;
}

public void printText( PrintService printer, String text2print ) { 
  JTextPane jtp = new JTextPane();
  jtp.setText( text2print );
  try {
    jtp.print(null, null, false, printer, null, false);
  } catch (java.awt.print.PrinterException ex) {
    ex.printStackTrace();
  }
}
